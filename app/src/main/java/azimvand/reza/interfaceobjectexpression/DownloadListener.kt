package azimvand.reza.interfaceobjectexpression

interface DownloadListener {
    fun onStart()
    fun downloading(percent: Int)
    fun onComplete(fileName: String)
}
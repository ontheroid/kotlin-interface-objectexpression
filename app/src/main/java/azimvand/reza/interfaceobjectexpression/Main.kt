package azimvand.reza.interfaceobjectexpression

fun main() {
    val downloader = Downloader("https://gitlab.ir/images/image1.jpg",
        object : DownloadListener {
            override fun onStart() {
                println("download is start.")
            }

            override fun downloading(percent: Int) {
                println("downloaded $percent %")
            }

            override fun onComplete(fileName: String) {
                println("$fileName downloaded.")
            }

        })
    downloader.start()
}